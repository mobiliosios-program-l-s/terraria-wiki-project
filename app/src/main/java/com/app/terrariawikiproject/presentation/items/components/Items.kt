package com.app.terrariawikiproject.presentation.items.components

import androidx.compose.runtime.Composable

import androidx.hilt.navigation.compose.hiltViewModel

import com.app.terrariawikiproject.domain.model.Response
import com.app.terrariawikiproject.components.ProgressBar
import com.app.terrariawikiproject.domain.repository.Items
import com.app.terrariawikiproject.presentation.items.ItemViewModel

@Composable
fun Items(
    viewModel: ItemViewModel = hiltViewModel(),
    itemsContent: @Composable (items: Items) -> Unit
) {
    when(val itemResponse = viewModel.itemResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> itemsContent(itemResponse.data)
        is Response.Failure -> print(itemResponse.e)
    }
}