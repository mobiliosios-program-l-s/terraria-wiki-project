package com.app.terrariawikiproject.presentation.items

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack

import androidx.navigation.NavController

import com.app.terrariawikiproject.presentation.items.components.Items
import com.app.terrariawikiproject.presentation.items.components.ItemsContent

@Composable
fun ItemScreen(
    navController: NavController,
    type: String?
) {
    Scaffold(
        topBar = {
                 TopAppBar(
                     title = {
                         Text(text = "Item Screen")
                     },
                     navigationIcon = {
                         IconButton(onClick = { navController.navigateUp() }) {
                             Icon(Icons.Filled.ArrowBack, null)
                         }
                     }
                 )
        },
        content = { padding ->
            val itemType = navController.currentBackStackEntry?.arguments?.getString("type")

            if(itemType.equals("Items")) {
                Items(
                    itemsContent = { items ->
                        ItemsContent(
                            padding = padding,
                            items = items,
                            navController = navController
                        )
                    }
                )
            } else {
                Items(
                    itemsContent = { items ->
                        if (type != null) {
                            ItemsContent(
                                padding = padding,
                                items = items.filter { x -> x.type == type.dropLast(1) },
                                navController = navController
                            )
                        }
                    }
                )
            }
        },
    )
}