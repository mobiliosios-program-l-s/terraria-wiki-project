package com.app.terrariawikiproject.presentation.items

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.terrariawikiproject.domain.model.Response
import com.app.terrariawikiproject.domain.repository.CraftIntoResponse
import com.app.terrariawikiproject.domain.repository.ItemResponse
import com.app.terrariawikiproject.domain.use_cases.UseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemViewModel @Inject constructor(private val useCases: UseCases): ViewModel() {
    var itemResponse by mutableStateOf<ItemResponse>(Response.Loading)
        private set
    var craftIntoResponse by mutableStateOf<CraftIntoResponse>(Response.Loading)
        private set

    init {
        getItems()
        getCraftInto()
    }

    private fun getItems() = viewModelScope.launch {
        useCases.getItems().collect { response ->
            itemResponse = response
        }
    }

    private fun getCraftInto() = viewModelScope.launch {
        useCases.getCraftInto().collect() { response ->
            craftIntoResponse = response
        }
    }
}