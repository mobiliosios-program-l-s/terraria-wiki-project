package com.app.terrariawikiproject.presentation.items.components

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import com.app.terrariawikiproject.components.ProgressBar
import com.app.terrariawikiproject.domain.model.Response
import com.app.terrariawikiproject.domain.repository.CraftItems
import com.app.terrariawikiproject.presentation.items.ItemViewModel

@Composable
fun Crafts(
    viewModel: ItemViewModel = hiltViewModel(),
    craftContent: @Composable (crafts: CraftItems) -> Unit
) {
    when(val craftResponse = viewModel.craftIntoResponse) {
        is Response.Loading -> ProgressBar()
        is Response.Success -> craftContent(craftResponse.data)
        is Response.Failure -> print(craftResponse.e)
    }
}