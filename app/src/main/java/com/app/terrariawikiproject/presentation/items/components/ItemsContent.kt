package com.app.terrariawikiproject.presentation.items.components

import androidx.compose.ui.Modifier

import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.PaddingValues

import androidx.compose.runtime.Composable
import androidx.navigation.NavController

import com.app.terrariawikiproject.domain.repository.Items

@Composable
fun ItemsContent(
    padding: PaddingValues,
    items: Items,
    navController: NavController
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize().padding(padding)
    ) {
        items(
            items = items
        ) { item ->
            ItemCard(
                item = item,
                navController = navController
            )
        }
    }
}