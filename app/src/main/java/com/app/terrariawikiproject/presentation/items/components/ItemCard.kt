package com.app.terrariawikiproject.presentation.items.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.fillMaxWidth

import androidx.compose.material.Text
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme

import androidx.compose.runtime.Composable

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.Modifier
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController

import coil.compose.AsyncImage
import coil.request.ImageRequest

import com.app.terrariawikiproject.domain.model.Item
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ItemCard(
    item: Item,
    navController: NavController
) {
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
            .padding(
                start = 8.dp,
                end = 8.dp,
                top = 4.dp,
                bottom = 4.dp
            )
            .fillMaxWidth(),
        elevation = 4.dp,
        onClick = {
            val encodedUrl = URLEncoder.encode(item.picture, StandardCharsets.UTF_8.toString())
            navController.navigate("info/${item.id}/${item.name}/${item.description}/$encodedUrl")
        }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(32.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            item.picture?.let {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(it)
                        .size(36, 36)
                        .build(),
                    contentDescription = null,
                    modifier = Modifier.size(26.dp)
                )
            }

            item.name?.let {
                Text(
                    text = it,
                    color = Color.White,
                    fontSize = 22.sp,
                    modifier = Modifier.padding(
                        start = 16.dp
                    )
                )
            }
        }
    }
}