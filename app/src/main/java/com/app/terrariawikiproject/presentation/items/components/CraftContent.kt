package com.app.terrariawikiproject.presentation.items.components

import androidx.compose.foundation.layout.*
import androidx.compose.ui.Modifier

import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text

import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController

import com.app.terrariawikiproject.domain.repository.CraftItems

@Composable
fun CraftContent(
    padding: PaddingValues,
    crafts: CraftItems,
    navController: NavController
) {
    if(crafts.isEmpty()) {
        Row(
            modifier = Modifier
                .padding(
                    start = 16.dp
                )
                .fillMaxWidth()
        ) {
            Text(
                text = "This item is not used in any crafting recipe!",
                fontSize = 12.sp,
                fontWeight = FontWeight.Bold
            )
        }
    } else {
        LazyColumn(
            modifier = Modifier
                .padding(padding)
                .fillMaxWidth()
        ) {
            items(
                items = crafts
            ) { item ->
                CraftCard(
                    item = item,
                    navController = navController
                )
            }
        }
    }
}