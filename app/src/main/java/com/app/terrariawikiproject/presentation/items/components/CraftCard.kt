package com.app.terrariawikiproject.presentation.items.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.app.terrariawikiproject.domain.model.CraftInto

@Composable
fun CraftCard(
    item: CraftInto,
    navController: NavController
) {
    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
            .padding(
                start = 8.dp,
                end = 8.dp,
                top = 4.dp,
                bottom = 4.dp
            )
            .fillMaxWidth(),
        elevation = 4.dp,
    ) {
        Row(
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth(),
        ) {
            item.output?.let {
                Text(
                    text = it,
                    color = Color.White,
                    fontSize = 22.sp,
                    modifier = Modifier
                        .padding(
                            start = 16.dp
                        )
                )
            }
        }
    }
}