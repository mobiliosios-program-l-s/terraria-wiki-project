package com.app.terrariawikiproject.presentation.items

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.app.terrariawikiproject.presentation.items.components.CraftContent
import com.app.terrariawikiproject.presentation.items.components.Crafts

@Composable
fun InfoScreen(
    navController: NavController,
    id: String?,
    name: String?,
    desc: String?,
    picture: String?
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Info Screen")
                },
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(Icons.Filled.ArrowBack, null)
                    }
                }
            )
        },
        content = { padding ->
            Column {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(50.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,
                ) {
                    if(picture != null) {
                        AsyncImage(
                            model = ImageRequest.Builder(LocalContext.current)
                                .data(picture)
                                .build(),
                            contentDescription = null,
                            modifier = Modifier.size(26.dp)
                        )
                    }

                    if (name != null) {
                        Text(
                            text = name,
                            fontSize = 22.sp,
                            modifier = Modifier.padding(
                                start = 10.dp
                            )
                        )
                    }
                }

                if (desc != null) {
                    Row(
                        modifier = Modifier
                            .padding(
                                start = 16.dp
                            )
                            .fillMaxWidth()
                    ) {
                        Text(
                            text = "Description:",
                            fontSize = 26.sp,
                            fontWeight = FontWeight.Bold
                        )
                    }

                    Row(
                        modifier = Modifier
                            .padding(
                                start = 16.dp
                            )
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceEvenly
                    ) {

                        Text(
                            text = desc,
                            fontSize = 22.sp
                        )
                    }
                }

                Row(
                    modifier = Modifier
                        .padding(
                            start = 16.dp
                        )
                        .fillMaxWidth()
                ) {
                    Text(
                        text = "Crafts into:",
                        fontSize = 26.sp,
                        fontWeight = FontWeight.Bold
                    )
                }

                Row(
                    modifier = Modifier
                        .padding(
                            start = 16.dp
                        )
                        .fillMaxWidth()
                ) {
                    Crafts(
                        craftContent = { crafts ->
                            CraftContent(
                                padding = padding,
                                crafts = crafts.filter { x -> x.input == id },
                                navController = navController)
                        }
                    )
                }
            }
        }
    )


}