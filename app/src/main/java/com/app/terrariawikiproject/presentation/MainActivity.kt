package com.app.terrariawikiproject.presentation

import android.os.Bundle

import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.app.terrariawikiproject.presentation.items.InfoScreen
import com.app.terrariawikiproject.presentation.items.ItemScreen

import dagger.hilt.android.AndroidEntryPoint

import com.app.terrariawikiproject.ui.theme.TerrariaWikiProjectTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TerrariaWikiProjectTheme {
                NavigationGraph()
            }
        }
    }
}

@Composable
private fun NavigationGraph() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "home" ) {
        composable("home") { HomeScreen(navController) }
        composable(
            route = "item/{type}",
            arguments = listOf(
                navArgument("type") { type = NavType.StringType },
            )
        ) { backStackEntry ->
            ItemScreen(
                navController = navController,
                type = backStackEntry.arguments?.getString("type")
            )
        }
        composable(
            route = "info/{id}/{name}/{desc}/{picture}",
            arguments = listOf(
                navArgument("id") { type = NavType.StringType },
                navArgument("name") { type = NavType.StringType },
                navArgument("desc") { type = NavType.StringType },
                navArgument("picture") { type = NavType.StringType}
            )
        ) { backStackEntry ->
            InfoScreen(
                navController = navController,
                id = backStackEntry.arguments?.getString("id"),
                name = backStackEntry.arguments?.getString("name"),
                desc = backStackEntry.arguments?.getString("desc"),
                picture = backStackEntry.arguments?.getString("picture")
            )
        }
    }
}