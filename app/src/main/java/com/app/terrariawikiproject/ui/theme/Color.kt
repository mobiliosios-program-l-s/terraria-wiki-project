package com.app.terrariawikiproject.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Gray_Dark = Color(0xFF5C5C5C)
val White = Color(0xFFFFFFFF)