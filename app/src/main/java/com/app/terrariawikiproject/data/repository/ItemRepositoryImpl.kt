package com.app.terrariawikiproject.data.repository

import com.app.terrariawikiproject.domain.model.CraftInto
import com.google.firebase.firestore.CollectionReference

import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.channels.awaitClose

import javax.inject.Inject

import com.app.terrariawikiproject.domain.model.Item
import com.app.terrariawikiproject.domain.model.Response
import com.app.terrariawikiproject.domain.repository.ItemRepository

class ItemRepositoryImpl @Inject constructor( private val itemRef: CollectionReference) : ItemRepository {
    override fun getItemsFromFirestore() = callbackFlow {
        val snapshotListener = itemRef.document("master").collection("item_list").orderBy("name").addSnapshotListener { snapshot, e ->
            val itemResponse = if (snapshot != null) {
                val items = snapshot.toObjects(Item::class.java)
                Response.Success(items)
            } else Response.Failure(e)
            trySend(itemResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override fun getCraftedItemsFromFirestore() = callbackFlow {
        val snapshotListener = itemRef.document("master").collection("craft_into").addSnapshotListener { snapshot, e ->
            val itemResponse = if (snapshot != null) {
                val craftInto = snapshot.toObjects(CraftInto::class.java)
                Response.Success(craftInto)
            } else Response.Failure(e)
            trySend(itemResponse)
        }
        awaitClose{
            snapshotListener.remove()
        }
    }
}