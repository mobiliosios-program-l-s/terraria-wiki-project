package com.app.terrariawikiproject.domain.model

data class Item (
    var id: String? = null,
    var name: String? = null,
    var description: String? = null,
    var type: String? = null,
    var picture: String? = null
)