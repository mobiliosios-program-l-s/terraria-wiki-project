package com.app.terrariawikiproject.domain.repository

import com.app.terrariawikiproject.domain.model.CraftInto
import kotlinx.coroutines.flow.Flow

import com.app.terrariawikiproject.domain.model.Item
import com.app.terrariawikiproject.domain.model.Response

typealias Items = List<Item>
typealias CraftItems = List<CraftInto>

typealias ItemResponse = Response<Items>
typealias CraftIntoResponse = Response<CraftItems>

interface ItemRepository {
    fun getItemsFromFirestore(): Flow<ItemResponse>
    fun getCraftedItemsFromFirestore(): Flow<CraftIntoResponse>
}