package com.app.terrariawikiproject.domain.use_cases

import com.app.terrariawikiproject.domain.repository.ItemRepository

class GetItems( private val repo: ItemRepository ) { operator fun invoke() = repo.getItemsFromFirestore() }