package com.app.terrariawikiproject.domain.use_cases

data class UseCases(
    val getItems: GetItems,
    val getCraftInto: GetCraftInto
)
