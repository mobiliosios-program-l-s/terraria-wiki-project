package com.app.terrariawikiproject.domain.model

data class CraftInto (
        var input: String? = null,
        var output: String? = null,
        var count: Int? = null
)