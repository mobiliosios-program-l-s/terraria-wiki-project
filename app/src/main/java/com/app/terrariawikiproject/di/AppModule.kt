package com.app.terrariawikiproject.di

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

import com.google.firebase.ktx.Firebase
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.CollectionReference

import com.app.terrariawikiproject.domain.use_cases.GetItems
import com.app.terrariawikiproject.domain.use_cases.UseCases
import com.app.terrariawikiproject.domain.repository.ItemRepository
import com.app.terrariawikiproject.data.repository.ItemRepositoryImpl
import com.app.terrariawikiproject.domain.use_cases.GetCraftInto

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideFirebaseFirestore() = Firebase.firestore

    @Provides
    @Singleton
    fun provideItemRef(
        db: FirebaseFirestore
    ) = db.collection("item")

    @Provides
    @Singleton
    fun provideItemRepository(
        itemRef: CollectionReference
    ): ItemRepository = ItemRepositoryImpl(itemRef)

    @Provides
    @Singleton
    fun provideUseCases(
        itemRepo: ItemRepository,
    ) = UseCases(
        getItems = GetItems(itemRepo),
        getCraftInto = GetCraftInto(itemRepo)
    )
}